﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableAudioEvent : ScriptableObject
{
    [SerializeField]
    private AudioClip[] _audioClips;

    [SerializeField]
    private float _volume;

    [Range(0.3f, 0.5f)]
    [SerializeField]
    private float _pitch;

    private float _maxVolume;
    private float _minVolume;

    public void Play(AudioSource source)
    {
        AudioClip clip = _audioClips[Random.Range(0, _audioClips.Length)];
        source.volume = _volume;
        source.pitch = _pitch;
        source.Play();
    }
}
