﻿using System;
using UEGP3.Core;
using UEGP3.InventorySystem;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

namespace UEGP3.PlayerSystem
{
	/// <summary>
	/// Player containing additional logic like item pick ups etc.
	/// </summary>
	public class Player : MonoBehaviour
	{
		[Tooltip("Inventory to be used for the player")] [SerializeField] 
		private Inventory _playerInventory;

		private bool _isInventoryOpen;

		private void Awake()
		{
			_isInventoryOpen = false;
		}

		private void Start()
		{
			_playerInventory.InstantiateCanvas();
		}
		private void Update()
		{
			// Show Inventory if button is pressed
			if (!_isInventoryOpen)
			{
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
				Time.timeScale = 1;
				if (Input.GetButtonDown("Inventory"))
				{
					_playerInventory.ShowInventory();
					_isInventoryOpen = true;
				}

			}

			else if(_isInventoryOpen)
			{
				Cursor.visible = true;
				Cursor.lockState = CursorLockMode.None;
				Time.timeScale = 0;
				if (Input.GetButtonDown("Inventory"))
				{
					_playerInventory.HideInventory();
					_isInventoryOpen = false;
				}
			}

			if (Input.GetButtonDown("ItemQuickAccess"))
			{
				_playerInventory.UseQuickAccessItem();
			}
		}

		public void UseIventoryItem()
		{
			Debug.Log("First click.");
			_playerInventory.UseItemInInventory();
		}

		public void Test()
		{
			Debug.Log("First click.");
		}

		private void OnTriggerEnter(Collider other)
		{
			// If we collide with a collectible item, collect it
			ICollectible collectible = other.gameObject.GetComponent<ICollectible>();
			if (collectible != null)
			{
				Collect(collectible);
			}
		}

		private void Collect(ICollectible collectible)
		{
			collectible.Collect(_playerInventory);
		}
	}
}